﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class Repository : IRepository
    {
        private readonly AppDbContext _ctx;

        public Repository(AppDbContext ctx)
        {
            _ctx = ctx;
        }

        public Person ReadPerson(int personId)
        {
            return _ctx.People
                //.Include(p => p.Courses)
                //    .ThenInclude(pc => pc.Course)
                .SingleOrDefault(p => p.PersonNumber == personId);
        }

        public IEnumerable<StudentCourse> ReadCoursesOfPerson(int personId)
        {
            var p = _ctx.People.Find(personId);
            _ctx.Entry(p).Collection(p => p.Courses).Load(); // explicit loading 'n'
            foreach (var pc in p.Courses)
            {
                _ctx.Entry(pc).Reference(pc => pc.Course).Load(); // explicit loading '(0..)1'
            }

            return p.Courses;
        }

        public IEnumerable<Person> ReadAllPeople(string name = null, int? age = null)
        {
            IQueryable<Person> result = _ctx.People;
            
            if (name != null)
                result = result.Where(p => p.Name.Contains(name));
            
            if (age != null)
                result = result.Where(p => p.Age >= age);
            
            return result;
        }
    }
}