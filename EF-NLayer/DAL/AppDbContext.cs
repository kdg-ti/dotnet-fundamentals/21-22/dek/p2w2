﻿using System.Collections.Generic;
using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;

namespace DAL
{
    public class AppDbContext : DbContext
    {
        public DbSet<Person> People { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Teacher> Teachers { get; set; }

        public DbSet<Course> Courses { get; set; }

        public DbSet<StudentCourse> StudentCourses { get; set; }
        
        // geen DbSet-prop voor owned-type(s) voorzien
        // anders toch aparte tabel voor owned-type (~table-splitting)
        // en is het relationeel terug 0..1-op-1
        //public DbSet<Address> Addresses { get; set; }

        public AppDbContext()
        {
            this.Database.EnsureDeleted();
            if (this.Database.EnsureCreated())
                Seed();
        }

        private void Seed()
        {
            var p1 = new Person()
            {
                Name = "Kenenth", Age = 41, 
                Address = new Address() { Street = "Nationalestraat", Number = 1, City = "Antwerpen"},
                Courses = new List<StudentCourse>()
            };
            this.People.Add(p1);
            
            var p3 = new Student()
            {
                Name = "Jef", Age = 23,
                Address = new Address() { Street = "Veldstraat", Number = 10, City = "Gent"},
                Email = "test@domain.tld",
                Courses = new List<StudentCourse>()
            };
            //var errors = new List<ValidationResult>();
            //bool valid = Validator.TryValidateObject(p3, new ValidationContext(p3), errors , validateAllProperties: true);
            var p2 = new Student()
            {
                Name = "Sabine", Age = 59,
                Address = new Address() { Street = "Nieuwstraat", Number = 5, City = "Brussel"},
                Email = "stud2@domain.tld",
                Courses = new List<StudentCourse>()
            };
            this.Students.Add(p3);
            this.Students.Add(p2);

            var p4 = new Teacher()
            {
                Name = "Robbe", Age = 20,
                Address = new Address() { Street = "Nationalestraat", Number = 5, City = "Antwerpen" },
                Subject = "omschrijving",
                Courses = new List<StudentCourse>()
            };
            this.Teachers.Add(p4);

            var c1 = new Course()
            {
                Name = ".NET", Students = new List<StudentCourse>()
            };
            var c2 = new Course()
            {
                Name = "Data Science", Students = new List<StudentCourse>()
            };
            this.Courses.Add(c1);
            this.Courses.Add(c2);

            var pc1 = new StudentCourse() { Course = c1, Student = p2 };
            c1.Students.Add(pc1);
            p2.Courses.Add(pc1);
            var pc2 = new StudentCourse() { Course = c2, Student = p3 };
            c2.Students.Add(pc2);
            p3.Courses.Add(pc2);
            this.StudentCourses.Add(pc1);
            this.StudentCourses.Add(pc2);

            this.SaveChanges();
            
            this.ChangeTracker.Clear();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //base.OnConfiguring(optionsBuilder);
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite("Data Source=myappdb.sqlite");

                optionsBuilder.UseLazyLoadingProxies(true);
                
                optionsBuilder.LogTo(msg => System.Diagnostics.Debug.WriteLine(msg), LogLevel.Information);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Owned<Address>();

            //modelBuilder.Entity<Person>().Property<int?>("FK_PersonId");
            /*modelBuilder.Entity<Person>()
                .HasOne(person => person.Address)
                .WithOne(address => address.Person)
                .HasForeignKey<Address>("FK_PersonId")
                .IsRequired(true);*/
            modelBuilder.Entity<Person>().OwnsOne(person => person.Address);
            
            modelBuilder.Entity<StudentCourse>()
                .HasOne(pc => pc.Student)
                .WithMany(p => p.Courses)
                .HasForeignKey("FK_StudentId")
                .IsRequired(true);
            modelBuilder.Entity<StudentCourse>()
                .HasOne(pc => pc.Course)
                .WithMany(c => c.Students)
                .HasForeignKey("FK_CourseId")
                .IsRequired(true);
            modelBuilder.Entity<StudentCourse>()
                .HasKey(new string[] { "FK_StudentId", "FK_CourseId" });

        }
    }
}