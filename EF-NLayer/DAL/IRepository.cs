﻿using System.Collections.Generic;
using Domain;

namespace DAL
{
    public interface IRepository
    {
        Person ReadPerson(int personId);
        IEnumerable<StudentCourse> ReadCoursesOfPerson(int personId);
        IEnumerable<Person> ReadAllPeople(string name = null, int? age = null);
    }
}