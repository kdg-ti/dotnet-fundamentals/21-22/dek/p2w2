﻿using System;
using System.Linq;
using BL;
using DAL;
using Domain;
using UI.CA.Extensions;

namespace UI.CA
{
    class Program
    {
        #region entry-point application + DI
        static void Main(string[] args)
        {
            AppDbContext ctx = new AppDbContext();
            IRepository repo = new Repository(ctx);
            IManager mgr = new Manager(repo);
            Program program = new Program(mgr);
            program.Run();
        }
        #endregion

        private readonly IManager _mgr;
        public Program(IManager mgr)
        {
            _mgr = mgr;
        }

        void Run()
        {
            Person p = _mgr.GetPerson(2);
            Console.WriteLine(p.GetInfo(showCourses:true));
            
            Console.WriteLine("\n***********");
            Person p2 = _mgr.GetPerson(3);
            var courses = _mgr.GetCoursesOfPerson(p2.PersonNumber);
            Console.WriteLine(p2.GetInfo(showCourses:true));

            Console.WriteLine("\n***********");
            var people = _mgr.GetAllPeople();

            foreach (var person in people)
            {
                //Console.WriteLine($"{person.Name} (age: {person.Age})" );
                Console.WriteLine(person.GetInfo());
            }
            
            /*
            var people1 = _mgr.GetAllPeople("e", 15);

            foreach (var person in people1)
            {
                Console.WriteLine(person.Name);
            }
            */
            
            /*
            var people2 = _mgr.GetAllPeople("e");
            people2 = people2.Where(p => p.Age >= 30);

            foreach (var person in people2)
            {
                Console.WriteLine(person.Name);
            }
            */

        }
    }
}