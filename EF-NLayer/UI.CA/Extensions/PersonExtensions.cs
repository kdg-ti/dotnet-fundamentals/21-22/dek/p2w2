﻿using System.Text;
using Domain;

namespace UI.CA.Extensions
{
    internal static class PersonExtensions
    {
        internal static string GetInfo(this Person person, bool showCourses = false)
        {
            string init = $"{person.Name} (age: {person.Age})";
            
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(init);

            if (showCourses && person.Courses != null)
            {
                sb.AppendLine($"\tCourses:");
                foreach (var course in person.Courses)
                {
                    if (course.Course != null)
                        sb.AppendLine($"\t{course.Course.Name}");
                }
            }

            return sb.ToString();
        }
    }
}