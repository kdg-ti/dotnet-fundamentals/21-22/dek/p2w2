﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class StudentCourse
    {
        [Key]
        [Required]
        public virtual Person Student { get; set; }
        [Key]
        [Required]
        public virtual Course Course { get; set; }

        public int Score { get; set; }
    }
}