﻿using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;
//using Microsoft.EntityFrameworkCore;

namespace Domain
{
    //[Owned]
    public class Address
    {
        //public int Id { get; set; }

        public string Street { get; set; }
        public int Number { get; set; }
        public string City { get; set; }

        [Required]
        public virtual Person Person { get; set; }

        //[ForeignKey("Person")]
        //public int PersonId { get; set; }
    }
}