﻿namespace Domain
{
    public class Teacher : Person
    {
        public string Subject { get; set; }
    }
}