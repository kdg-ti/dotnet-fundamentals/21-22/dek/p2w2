﻿namespace Domain
{
    public class Student : Person
    {
        public string Email { get; set; }
    }
}