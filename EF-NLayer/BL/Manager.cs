﻿using System.Collections.Generic;
using DAL;
using Domain;

namespace BL
{
    public class Manager : IManager
    {
        private readonly IRepository _repo;

        public Manager(IRepository repo)
        {
            _repo = repo;
        }

        public Person GetPerson(int personId)
        {
            return _repo.ReadPerson(personId);
        }

        public IEnumerable<StudentCourse> GetCoursesOfPerson(int personId)
        {
            return _repo.ReadCoursesOfPerson(personId);
        }

        public IEnumerable<Person> GetAllPeople(string name = null, int? age = null)
        {
            return _repo.ReadAllPeople(name, age);
        }
    }
}