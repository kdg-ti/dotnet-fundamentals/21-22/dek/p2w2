﻿using System.Collections.Generic;
using Domain;

namespace BL
{
    public interface IManager
    {
        Person GetPerson(int personId);
        IEnumerable<StudentCourse> GetCoursesOfPerson(int personId);
        IEnumerable<Person> GetAllPeople(string name = null, int? age = null);
    }
}